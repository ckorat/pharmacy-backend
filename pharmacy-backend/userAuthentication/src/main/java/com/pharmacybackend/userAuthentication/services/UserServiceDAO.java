package com.pharmacybackend.userAuthentication.services;
import com.pharmacybackend.userAuthentication.dao.UserService;
import com.pharmacybackend.userAuthentication.model.AdminModel;
import com.pharmacybackend.userAuthentication.model.AppUserModel;
import com.pharmacybackend.userAuthentication.model.StoreModel;
import com.pharmacybackend.userAuthentication.model.UserModel;
import com.pharmacybackend.userAuthentication.repository.AdminRepository;
import com.pharmacybackend.userAuthentication.repository.StoreRepository;
import com.pharmacybackend.userAuthentication.repository.UserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
@Data
@Service
public class UserServiceDAO implements UserService {
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private StoreRepository storeRepository;
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private BCryptPasswordEncoder encoder;
    List<AppUserModel> model = new ArrayList<>();

    @Override
    public List<AppUserModel> getAllUser() {
        List<UserModel> userModels = userRepo.findAll();
        List<StoreModel> storeModels = storeRepository.findAll();
        List<AdminModel> adminModels = adminRepository.findAll();
        for (AdminModel admin : adminModels) {
            model.add(
                    new AppUserModel(admin.getAdminId(), admin.getAdminEmail(), encoder.encode(admin.getAdminPassword()), "ADMIN"));
        }
        for (UserModel user : userModels) {
            model.add(
                    new AppUserModel(user.getUserId(), user.getEmail(), encoder.encode(user.getPassword()), "USER"));
        }
        for (StoreModel store : storeModels) {
            model.add(
                    new AppUserModel(store.getStoreId(), store.getStoreEmailId(), encoder.encode(store.getStorePassword()), "STORE"));
        }
        return model;
    }
}
