package com.pharmacybackend.userAuthentication.config;
import javax.servlet.http.HttpServletResponse;

import com.pharmacybackend.userAuthentication.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
@EnableWebSecurity
public class SecurityCredentialsConfig  extends WebSecurityConfigurerAdapter  {
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtConfig jwtConfig;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
                http
                                .csrf().disable()
                                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                                .and()
                                .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                                .and()
                                .addFilterAfter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager(), jwtConfig,getUserDetailsServiceImpl()), UsernamePasswordAuthenticationFilter.class)
                                .addFilterAfter(new JwtTokenAuthenticationFilter(jwtConfig),
                                        UsernamePasswordAuthenticationFilter.class)
                                .authorizeRequests()
                                .antMatchers(HttpMethod.POST, jwtConfig.getUri()).permitAll()
                                .antMatchers("/query/queries").hasAnyRole("ADMIN","USERS")
                                .antMatchers("/user/users").hasRole("USER")
                                .anyRequest().authenticated();
            }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
                auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
            }
    @Bean
    public JwtConfig jwtConfig() {
                return new JwtConfig();
            }
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
                return new BCryptPasswordEncoder();
            }

    @Bean
    public UserDetailsServiceImpl getUserDetailsServiceImpl() {
        return new UserDetailsServiceImpl();
    }

}
