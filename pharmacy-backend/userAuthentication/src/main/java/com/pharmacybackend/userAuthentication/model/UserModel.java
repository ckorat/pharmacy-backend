package com.pharmacybackend.userAuthentication.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
@Data
@Entity
@Table(name = "userdetails")
@NoArgsConstructor
@AllArgsConstructor
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userid")
    private int userId;
    @Column(name = "username")
    private String userName;
    @Column(name = "userfullname")
    private String name;
    @Column(name = "useremailid")
    private String email;
    @Column(name = "userpassword")
    private String password;
    @Column(name = "usercontactno")
    private String contactNo;
    @Column(name = "useraddress")
    private String address;
    @Column(name = "usercity")
    private String userCity;
    @Column(name = "userstate")
    private String userState;
    @Column(name = "userpincode")
    private int userPinCode;
}
