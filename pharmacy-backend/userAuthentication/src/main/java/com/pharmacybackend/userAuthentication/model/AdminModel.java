package com.pharmacybackend.userAuthentication.model;
import lombok.Data;
import javax.persistence.*;
@Data
@Entity
@Table(name = "admindetails")
public class AdminModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "adminid")
    private int adminId;
    @Column(name = "adminemailid")
    private String adminEmail;
    @Column(name = "adminpassword")
    private String adminPassword;
}
