package com.pharmacybackend.userAuthentication.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "storedetails")
public class StoreModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "storeid")
    private int storeId;
    @Column(name = "storename")
    private String  storeName;
    @Column(name = "storeownername")
    private String storeOwnerName;
    @Column(name = "storeemailid")
    private String storeEmailId;
    @Column(name = "storepassword")
    private String storePassword;
    @Column(name = "storeaddress")
    private String storeAddress;
    @Column(name = "storepincode")
    private int storePinCode;
    @Column(name = "storecontactno")
    private String storeContactNo;
    @Column(name = "storestatus")
    private boolean storeStatus;
}
