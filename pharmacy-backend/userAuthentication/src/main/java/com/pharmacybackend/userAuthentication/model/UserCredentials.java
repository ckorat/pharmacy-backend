package com.pharmacybackend.userAuthentication.model;
import lombok.Data;
@Data
public class UserCredentials {
    private String userEmail, userPassword;
}