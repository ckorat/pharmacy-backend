package com.pharmacybackend.userAuthentication.repository;

import com.pharmacybackend.userAuthentication.model.StoreModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface StoreRepository extends JpaRepository<StoreModel,Integer> {
}
