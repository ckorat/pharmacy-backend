package com.pharmacybackend.userAuthentication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@EnableZuulProxy
public class UserAuthenticationApplication {
	public static void main(String[] args) {
		SpringApplication.run(UserAuthenticationApplication.class, args);
	}
}
