package com.pharmacybackend.userAuthentication.services;
import java.util.ArrayList;
import java.util.List;
import com.pharmacybackend.userAuthentication.model.AppUserModel;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
@Data
@Service
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private UserServiceDAO userServiceDAO;
    List<AppUserModel> users=new ArrayList<>();
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        users=userServiceDAO.getAllUser();
        for(AppUserModel appUser: users) {
            if(appUser.getUsername().equals(username)) {
                List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                        .commaSeparatedStringToAuthorityList("ROLE_"+  appUser.getRole());
                return new User(appUser.getUsername(), appUser.getPassword(), grantedAuthorities);
            }
        }
            throw new UsernameNotFoundException("Username: "  +username + " not found");
    }
    public List<AppUserModel> getAllUser(){
        return users;
    }

    public AppUserModel getUserByUserName(String username) {
        users=userServiceDAO.getAllUser();
        for (AppUserModel user : users) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
    return  null;
    }
}

