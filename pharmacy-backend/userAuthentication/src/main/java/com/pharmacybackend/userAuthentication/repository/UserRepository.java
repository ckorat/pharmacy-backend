package com.pharmacybackend.userAuthentication.repository;

import com.pharmacybackend.userAuthentication.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<UserModel,Integer> {
}
