package com.pharmacybackend.userAuthentication.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppUserModel {
    private Integer id;
    private String username;
    @JsonIgnore
    private String password;
    private String role;

    @Override
    public String toString() {
        return "id:"+getId()+","+"username:"+getUsername()+","+"role:"+getRole();
    }
}
