package com.pharmacybackend.userAuthentication.repository;

import com.pharmacybackend.userAuthentication.model.AdminModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AdminRepository extends JpaRepository<AdminModel,Integer> {
}
