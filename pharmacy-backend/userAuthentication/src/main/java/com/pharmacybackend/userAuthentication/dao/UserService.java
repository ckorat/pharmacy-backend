package com.pharmacybackend.userAuthentication.dao;
import com.pharmacybackend.userAuthentication.model.AppUserModel;
import java.util.List;
public interface UserService {
    List<AppUserModel> getAllUser();
}
