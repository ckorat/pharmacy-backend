package com.pharmacybackend.userAuthentication.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pharmacybackend.userAuthentication.model.AppUserModel;
import com.pharmacybackend.userAuthentication.model.AuthenticationResponse;
import com.pharmacybackend.userAuthentication.model.UserCredentials;
import com.pharmacybackend.userAuthentication.services.UserDetailsServiceImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.Collections;
import java.util.stream.Collectors;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private UserDetailsServiceImpl userDetailsService;
    private AuthenticationManager authManager;
    private final JwtConfig jwtConfig;
    private ObjectMapper mapper = new ObjectMapper();

    public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authManager, JwtConfig jwtConfig, UserDetailsServiceImpl userDetailsService) {
        this.authManager = authManager;
        this.jwtConfig = jwtConfig;
        this.userDetailsService = userDetailsService;
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(jwtConfig.getUri(), "POST"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            UserCredentials creds = new ObjectMapper().readValue(request.getInputStream(), UserCredentials.class);
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    creds.getUserEmail(), creds.getUserPassword(), Collections.emptyList());
            return authManager.authenticate(authToken);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        Long now = System.currentTimeMillis();
        String token = Jwts.builder()
                .setSubject(auth.getName())
                .claim("authorities", auth.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + jwtConfig.getExpiration() * 1000))
                .signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret().getBytes())
                .compact();
        response.addHeader(jwtConfig.getHeader(), jwtConfig.getPrefix() + " " + token);
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        AppUserModel authUser = userDetailsService.getUserByUserName(auth.getName());
        response.getWriter().write(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(new AuthenticationResponse(authUser.getId(), authUser.getUsername(), authUser.getRole())));
    }
}
