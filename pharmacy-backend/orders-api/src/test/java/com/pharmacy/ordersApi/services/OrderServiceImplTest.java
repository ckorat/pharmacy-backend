package com.pharmacy.ordersApi.services;

import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.ordersApi.model.OrderModel;
import com.pharmacy.ordersApi.repository.OrderRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OrderServiceImplTest {

    OrderRepository orderRepository = mock(OrderRepository.class);
    OrderServiceImpl orderService;

    @Before
    public void setUp() {
        orderService = new OrderServiceImpl(orderRepository);
    }

    @Test
    public void getAllOrdersSuccess() throws Exception {
        List<OrderModel> orderModel = new ArrayList<>();
        OrderModel orderModel1 = new OrderModel(37, 3, 4, 1, 2, "REQUESTED", Date.valueOf("2020-03-05"), 200, "csdvsdvsd", 395009);
        orderModel.add(orderModel1);
        when(orderRepository.findAll()).thenReturn(orderModel);
        List<OrderModel> orderModelList = orderService.getAllOrders();
        Assert.assertEquals(orderModel, orderModelList);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void getOrderByIdFailure() throws Exception {
        orderService.getOrdersById(0, null);
    }
}
