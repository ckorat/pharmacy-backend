package com.pharmacy.ordersApi.services;

import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.exception.TechnicalException;
import com.pharmacy.ordersApi.dao.OrderService;
import com.pharmacy.ordersApi.dto.ResponseDTO;
import com.pharmacy.ordersApi.model.OrderModel;
import com.pharmacy.ordersApi.model.OrderStatus;
import com.pharmacy.ordersApi.model.Type;
import com.pharmacy.ordersApi.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service("orderServiceImpl")
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    /*Fetching all orders details*/
    @Override
    public List<OrderModel> getAllOrders() throws Exception {

        List<OrderModel> orderModelList = orderRepository.findAll();
        if (orderModelList.isEmpty())
            throw new TechnicalException("Some technical error.");
        return orderModelList;
    }

    /*Orders details by Id of (OrderId, StoreId, UserId)*/
    public List<OrderModel> getOrdersById(int id, Type type) throws Exception {
        Optional<OrderModel> orderModel = orderRepository.findById(id);
        if (orderModel.isPresent()) {
            return type == Type.STORE ? orderRepository.findByStoreId(id) : type == Type.USER
                    ? orderRepository.findByUserId(id) : orderRepository.findAllById(Collections.singleton(id));
        } else
            throw new ResourceNotFoundException("No orders details found.");
    }

    /*Order is been placed and Status is set to REQUESTED*/
    @Override
    public ResponseDTO placeOrder(OrderModel orderModel) throws MethodArgumentNotValidException {
        if (orderModel != null) {
            orderModel.setOrderStatus(String.valueOf(OrderStatus.REQUESTED));
            orderRepository.save(orderModel);
            return new ResponseDTO("Order Placed Successfully", orderModel);
        } else
            return new ResponseDTO("Order is not placed.");
    }

    /*Order status updated as per the id*/
    @Override
    public ResponseDTO updateStatus(int orderId, OrderStatus orderStatus) throws Exception {
        return orderRepository.findById(orderId).map(orderModel -> {
            orderModel.setOrderStatus(orderStatus.toString());
            orderRepository.save(orderModel);
            return new ResponseDTO("Status Updated", orderModel);
        }).orElseThrow(() -> new ResourceNotFoundException("Order Id " + orderId + " not found"));
    }
}
