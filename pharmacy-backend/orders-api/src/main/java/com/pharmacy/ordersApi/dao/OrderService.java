package com.pharmacy.ordersApi.dao;

import com.pharmacy.ordersApi.dto.ResponseDTO;
import com.pharmacy.ordersApi.model.OrderModel;
import com.pharmacy.ordersApi.model.OrderStatus;
import com.pharmacy.ordersApi.model.Type;

import java.util.List;

public interface OrderService {
    List<OrderModel> getAllOrders() throws Exception;

    List<OrderModel> getOrdersById(int id, Type type) throws Exception;

    ResponseDTO placeOrder(OrderModel orderModel) throws Exception;

    ResponseDTO updateStatus(int orderId, OrderStatus orderStatus) throws Exception;
}
