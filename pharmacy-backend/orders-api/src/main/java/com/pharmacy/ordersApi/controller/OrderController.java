package com.pharmacy.ordersApi.controller;

import com.pharmacy.ordersApi.dao.OrderService;
import com.pharmacy.ordersApi.dto.ResponseDTO;
import com.pharmacy.ordersApi.model.OrderModel;
import com.pharmacy.ordersApi.model.OrderStatus;
import com.pharmacy.ordersApi.model.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping("")
    public ResponseEntity<ResponseDTO> addOrderDetails(@RequestBody OrderModel orderModel) throws Exception {

        return new ResponseEntity<>(orderService.placeOrder(orderModel), HttpStatus.CREATED);
    }

    @PutMapping("/{orderId}/{orderStatus}")
    public ResponseEntity<ResponseDTO> updateStatus(@PathVariable(value = "orderId") int orderId, @PathVariable OrderStatus orderStatus) throws Exception {
        return new ResponseEntity<>(orderService.updateStatus(orderId, orderStatus), HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<ResponseDTO<List<OrderModel>>> getAllOrders() throws Exception {
        return new ResponseEntity<>(new ResponseDTO<>(orderService.getAllOrders()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO<List<OrderModel>>> getOrdersById(@PathVariable(value = "id", required = false) Integer id, @RequestParam(value = "type", required = false) Type type) throws Exception {
        return new ResponseEntity<>(new ResponseDTO<>(orderService.getOrdersById(id, type)), HttpStatus.OK);
    }
}
