package com.pharmacy.ordersApi.repository;

import com.pharmacy.ordersApi.model.OrderModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<OrderModel, Integer> {
    List<OrderModel> findByUserId(int userId);

    List<OrderModel> findByStoreId(int storeId);
}
