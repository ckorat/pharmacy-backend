package com.pharmacy.ordersApi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.Min;
import java.sql.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orderdetails")
public class OrderModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "orderid")
    private int orderId;

    @Column(name = "storeid")
    private int storeId;

    @Column(name = "userid")
    private int userId;

    @Column(name = "medicineid")
    private int medicineId;

    @Column(name = "orderquantity")
    private int orderQuantity;

    @Column(name = "orderstatus")
    private String orderStatus;

    @Column(name = "orderdate")
    private Date orderDate;

    @Column(name = "orderamount")
    private int orderAmount;

    @Column(name = "useraddress")
    private String userAddress;

    @Min(6)
    @Column(name = "userpincode")
    private int userPincode;
}
