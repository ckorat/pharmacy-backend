package com.pharmacy.ordersApi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class ResponseDTO<T> {
    private String message;
    private T result;

    public ResponseDTO(String message) {
        this.message = message;
    }

    public ResponseDTO(T result) {
        this.result = result;
    }

}

