package com.pharmacy.ordersApi.model;

public enum OrderStatus {
    REQUESTED,
    REJECTED,
    COMPLETED
}
