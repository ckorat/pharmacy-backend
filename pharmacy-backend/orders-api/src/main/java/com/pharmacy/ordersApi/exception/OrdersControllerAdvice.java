package com.pharmacy.ordersApi.exception;

import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.exception.TechnicalException;
import com.pharmacy.ordersApi.dto.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class OrdersControllerAdvice {

        @ResponseStatus(HttpStatus.BAD_REQUEST)
        @ResponseBody
        @ExceptionHandler(MethodArgumentNotValidException.class)
        public ResponseDTO handleMethodArgumentNotValid(MethodArgumentNotValidException ex)
        {
            List<String> details = new ArrayList<>();
            for(ObjectError error : ex.getBindingResult().getAllErrors()) {
                details.add(error.getDefaultMessage());
            }
            return new ResponseDTO("Validation Failed", details);
        }

        @ResponseStatus(HttpStatus.NOT_FOUND)
        @ResponseBody
        @ExceptionHandler(ResourceNotFoundException.class)
        public ResponseDTO handleResourceNotFoundExceptions(ResourceNotFoundException ex)
        {
            return new ResponseDTO("error",ex.getMessage());
        }

        @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
        @ResponseBody
        @ExceptionHandler(TechnicalException.class)
        public ResponseDTO handleTechnicalExceptions(TechnicalException ex)
        {
            return new ResponseDTO("error",ex.getMessage());
        }

    }

