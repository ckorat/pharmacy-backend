package com.pharmacybackend.userApi.controller;
import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.exception.TechnicalException;
import com.pharmacybackend.userApi.dto.ResponseDTO;
import com.pharmacybackend.userApi.model.UserPassword;
import com.pharmacybackend.userApi.service.UserServiceDaoImpl;
import com.pharmacybackend.userApi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = "/users",produces = {MediaType.APPLICATION_JSON_VALUE})
public class UserController {

    @Autowired
    private UserServiceDaoImpl userService;

    @GetMapping("/")
    public ResponseEntity<ResponseDTO<List<User>>> getAllUser() throws TechnicalException
    {
        return new ResponseEntity<>( new ResponseDTO<>(userService.getAllUser()), HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<ResponseDTO<User>> getUser(@PathVariable(name = "userId") int userId) throws ResourceNotFoundException
    {
        return new ResponseEntity<>(new ResponseDTO<>( userService.getUserById(userId)),HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<ResponseDTO> addUser(@RequestBody @Valid User user)
    {
       return new ResponseEntity<>( userService.addUser(user),HttpStatus.CREATED);
    }

    @PutMapping("/")
    public ResponseEntity<ResponseDTO> updateUser(@RequestBody @Valid User user)throws ResourceNotFoundException
    {
        return new ResponseEntity<>( userService.updateUserDetail(user),HttpStatus.OK);
    }

    @PutMapping("/{userId}")
    public ResponseEntity<ResponseDTO> changePassword(@RequestBody @Valid UserPassword data, @PathVariable(name = "userId") int userId)throws ResourceNotFoundException{
        return new ResponseEntity<>(userService.updateUserPassword(data,userId),HttpStatus.OK);
    }
}
