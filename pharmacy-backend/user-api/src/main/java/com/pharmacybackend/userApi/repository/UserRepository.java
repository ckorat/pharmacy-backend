package com.pharmacybackend.userApi.repository;

import com.pharmacybackend.userApi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {

}
