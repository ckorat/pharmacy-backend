package com.pharmacybackend.userApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "userdetails")
public class User {

    @Id
    @Column(name = "userid")
    @JsonProperty(value = "userid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public int userId;

    @Column(name = "username")
    @JsonProperty(value = "name")
    @NotBlank(message = "Name must not empty")
    public String userName;

    @Column(name = "userfullname")
    @JsonProperty(value = "fullName")
    @NotBlank(message = "Full Name must not empty")
    public String userFullName;

    @Column(name = "useremailid")
    @JsonProperty(value = "email")
    @NotBlank(message = "Email must not empty")
    public String userEmailId;

    @Column(name = "userpassword")
    @JsonProperty(value="password",access = JsonProperty.Access.WRITE_ONLY)
    public String userPassword;

    @Column(name = "usercontactno")
    @JsonProperty(value = "contact")
    @NotBlank(message = " Contact must not empty")
    public String userContactNo;

    @Column(name = "useraddress")
    @JsonProperty(value = "address")
    @NotBlank(message = "Address must not empty")
    public String userAddress;

    @Column(name = "usercity")
    @JsonProperty(value = "city")
    @NotBlank(message = "City must not empty")
    public String userCity;

    @Column(name = "userstate")
    @JsonProperty(value = "state")
    @NotBlank(message = "State must not empty")
    public String userState;

    @Column(name = "userpincode")
    @JsonProperty(value = "pincode")
    @NotNull(message = "Pincode must not empty")
    public int userPincode;

    public User(int userId,String userName,String userFullName,String userEmailId,String userContactNo,String userAddress,String userCity,String userState,int userPincode) {
        this.userId=userId;
        this.userName=userName;
        this.userFullName=userFullName;
        this.userEmailId=userEmailId;
        this.userContactNo=userContactNo;
        this.userAddress=userAddress;
        this.userCity=userCity;
        this.userState=userState;
        this.userPincode=userPincode;
    }
    public User(String userName,String userFullName,String userEmailId,String userContactNo,String userAddress,String userCity,String userState,int userPincode) {
        this.userName=userName;
        this.userFullName=userFullName;
        this.userEmailId=userEmailId;
        this.userContactNo=userContactNo;
        this.userAddress=userAddress;
        this.userCity=userCity;
        this.userState=userState;
        this.userPincode=userPincode;
    }


    public User() {
    }

}
