package com.pharmacybackend.userApi.service;

import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.exception.TechnicalException;
import com.pharmacybackend.userApi.dao.UserServiceDao;
import com.pharmacybackend.userApi.dto.ResponseDTO;
import com.pharmacybackend.userApi.model.User;
import com.pharmacybackend.userApi.model.UserPassword;
import com.pharmacybackend.userApi.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceDaoImpl implements UserServiceDao {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAllUser() throws TechnicalException{
        try{
            return userRepository.findAll();

        }catch (Exception e){
            throw new TechnicalException("somthing wrong");
        }
    }

    @Override
    public User getUserById(int id) throws ResourceNotFoundException {
        return userRepository.findById(id)
                .orElseThrow( ()-> new ResourceNotFoundException("user Not found"));
    }

    @Override
    public ResponseDTO addUser(User user){
        userRepository.save(user);
       return new ResponseDTO("Registration SuccessFull");
    }

    @Override
    public ResponseDTO updateUserDetail(User user) throws ResourceNotFoundException{
        User updateUser=userRepository.findById(user.getUserId())
                .orElseThrow(()->new ResourceNotFoundException("user not found"));

        user.setUserEmailId(updateUser.getUserEmailId());
        user.setUserPassword(updateUser.getUserPassword());
        userRepository.save(user);
        return new ResponseDTO("Profile Updated",user);
    }

    @Override
    public ResponseDTO updateUserPassword(UserPassword data, int id)throws ResourceNotFoundException {
    return  userRepository.findById(id).map(user ->{
           if(user.getUserPassword().equals(data.getOldPassword())){
               user.setUserPassword(data.getNewPassword());
               userRepository.save(user);
               return  new ResponseDTO( "Password updated");
           }
                return new ResponseDTO("Old Password Is Wrong");
            }
       ).orElseThrow(()->new ResourceNotFoundException("user not found"));

    }


}
