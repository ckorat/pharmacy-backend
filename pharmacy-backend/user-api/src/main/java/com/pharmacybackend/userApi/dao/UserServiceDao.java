package com.pharmacybackend.userApi.dao;

import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacybackend.userApi.dto.ResponseDTO;
import com.pharmacybackend.userApi.model.User;
import com.pharmacybackend.userApi.model.UserPassword;
import java.util.List;

public interface UserServiceDao {
     List<User>getAllUser() throws Exception;
     User getUserById(int id) throws ResourceNotFoundException;
     ResponseDTO addUser(User user) throws Exception;
     ResponseDTO updateUserDetail(User user)throws Exception;
     ResponseDTO updateUserPassword(UserPassword data, int id)throws Exception;
}
