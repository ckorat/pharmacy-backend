package com.pharmacybackend.userApi.service;
import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.exception.TechnicalException;
import com.pharmacybackend.userApi.dto.ResponseDTO;
import com.pharmacybackend.userApi.model.User;
import com.pharmacybackend.userApi.model.UserPassword;
import com.pharmacybackend.userApi.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;

public class UserServiceTest {
    UserServiceDaoImpl userServiceDao;
    UserRepository userRepository= Mockito.mock(UserRepository.class);

    @Before
    public void  setUp(){

        userServiceDao=new UserServiceDaoImpl(userRepository);
    }
    @Test
    public void getAllUserSuccess() throws TechnicalException {
        List<User> users=new ArrayList<>();
        User user1=new User(7,"ronish","Ronish1","roni@yahoo.com","9876543210","Surat","Surat","Gujarat",394210);
        users.add(user1);
        Mockito.when(userRepository.findAll()).thenReturn(users);
       List<User> userList= userServiceDao.getAllUser();
        Assert.assertEquals(users,userList);
    }

    @Test
    public void getUserByIdSuccess()throws ResourceNotFoundException {
        User user=new User(7,"ronish","Ronish1","roni@yahoo.com","9876543210","Surat","Surat","Gujarat",394210);
        Mockito.when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        User user1=userServiceDao.getUserById(1);
        Assert.assertEquals(user,user1);
    }

    @Test
    public void addUserSuccess(){
        ResponseDTO responseDTO=new ResponseDTO("Registration SuccessFull");
        User user=new User("ronish","Ronish1","roni@yahoo.com","9876543210","Surat","Surat","Gujarat",394210);
        Mockito.when(userRepository.save(any())).thenReturn(user);
        ResponseDTO responseDTO1=userServiceDao.addUser(user);
        Assert.assertEquals(responseDTO,responseDTO1);
    }

    @Test
    public void updateUserDetaileSuccess()throws ResourceNotFoundException{

        User user=new User(7,"ronish","Ronish1","roni@yahoo.com","9876543210","Surat","Surat","Gujarat",394210);
        ResponseDTO responseDTO=new ResponseDTO("Profile Updated",user);
        Mockito.when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        Mockito.when(userRepository.save(any())).thenReturn(user);
        ResponseDTO responseDTO1=userServiceDao.updateUserDetail(user);
        Assert.assertEquals(responseDTO,responseDTO1);
    }

    @Test
    public void updateUserPasswordSuccess()throws ResourceNotFoundException{
        UserPassword userPassword=new UserPassword("1234","12345");
        User user=new User(7,"ronish","Ronish1","roni@yahoo.com","9876543210","Surat","Surat","Gujarat",394210);
        ResponseDTO responseDTO=new ResponseDTO( "Password updated");
        Mockito.when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        user.setUserPassword(userPassword.getOldPassword());
        Mockito.when(userRepository.save(any())).thenReturn(user);
        ResponseDTO responseDTO1=userServiceDao.updateUserPassword(userPassword,7);
        Assert.assertEquals(responseDTO,responseDTO1);
    }
}
