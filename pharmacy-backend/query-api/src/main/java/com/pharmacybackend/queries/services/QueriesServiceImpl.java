package com.pharmacybackend.queries.services;

import java.util.List;

import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacybackend.queries.DTO.ResponseDTO;
import com.pharmacybackend.queries.exception.MissingInfoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pharmacybackend.queries.DAO.QueriesDAO;
import com.pharmacybackend.queries.model.Queries;

@Service
public class QueriesServiceImpl implements QueriesServices {

	@Autowired
	public QueriesDAO queriesDAO;

	public List<Queries> getQueries() {
		return  queriesDAO.findByQueryStatus(0);
	}
	public ResponseDTO addQueries(Queries query) throws MissingInfoException {
		 if ( queriesDAO.save(query) != null) { return new ResponseDTO("Query Successfully Submitted"); }
		throw new MissingInfoException("Missing Query Details");
	}
	public ResponseDTO updateQuery(Queries query, Integer queryId) throws Exception{
		if(query.getQueryReply().isEmpty()) {
			throw new MissingInfoException("Can not submit empty reply");
		}
		return queriesDAO.findById(queryId).map(object -> {
			object.setQueryReply(query.getQueryReply());
			object.setQueryStatus(1);
			return new ResponseDTO("Reply Submitted", queriesDAO.save(object));
		}).orElseThrow(()-> {return new ResourceNotFoundException("Query Record Not Found");});
	}
}
