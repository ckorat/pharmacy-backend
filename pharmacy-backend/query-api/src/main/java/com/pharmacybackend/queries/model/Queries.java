package com.pharmacybackend.queries.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.sql.Date;

@Entity
@Data
@Table(name = "queriesdetails")
public class Queries {

	@Id
	@Column(name="queryid")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@JsonProperty("id")
	private Integer queryId;
	
	@Column(name="queryfullname")
	@JsonProperty("fullname")
	private String  queryFullname;
	
	@Column(name="queryemailid")
	@Email(message = "Invalid Email")
	@JsonProperty("email")
	private String  queryEmailid;
	
	@Column(name="querycontactno")
	@JsonProperty("contactno")
	private String  queryContactno;
	
	@Column(name="querytopic")
	@JsonProperty("topic")
	private String  queryTopic;
	
	@Column(name="querydescription")
	@JsonProperty("description")
	private String  queryDescription;
	
	@Column(name="querystatus")
	@JsonProperty("status")
	private Integer queryStatus;
	
	@Column(name="queryreply")
	@JsonProperty("reply")
	private String  queryReply;
	
	@Column(name="queryfor")
	@JsonProperty("type")
	private String  queryFor;

	@Column(name = "querydate")
	@JsonProperty("date")
	private Date queryDate;
}
