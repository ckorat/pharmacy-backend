package com.pharmacybackend.queries.exception;


public class MissingInfoException extends Exception
{
    private static final long serialVersionUID = 1L;
 
    public MissingInfoException(String message) {
        super(message);
    }
}