package com.pharmacybackend.queries.exception;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponse
{
      private String message;
    private List<String> details;
	@Override
	public String toString() {
		return "ErrorResponse [message=" + message + ", details=" + details + "]";
	}   
    

}