package com.pharmacybackend.queries.exception;

import java.util.ArrayList;
import java.util.List;

import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.exception.TechnicalException;
import com.pharmacybackend.queries.DTO.ResponseDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class QueriesExceptionsController extends ResponseEntityExceptionHandler {

	@Override
	public final ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
																	 HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getMessage());
		return new ResponseEntity<>(new ResponseDTO("Invalid Request", details), HttpStatus.BAD_REQUEST);
	}


	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseDTO handleResourceNotFoundExceptions(ResourceNotFoundException ex)
	{
		return new ResponseDTO("error",ex.getMessage());
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	@ExceptionHandler(TechnicalException.class)
	public ResponseDTO handleTechnicalExceptions(TechnicalException ex)
	{
		return new ResponseDTO("error",ex.getMessage());
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	@ExceptionHandler(MissingInfoException.class)
	public ResponseDTO handleTechnicalExceptions(MissingInfoException ex)
	{
		return new ResponseDTO("error",ex.getMessage());
	}
}
