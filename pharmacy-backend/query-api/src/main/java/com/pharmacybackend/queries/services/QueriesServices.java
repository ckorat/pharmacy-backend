package com.pharmacybackend.queries.services;


import java.util.List;

import com.pharmacybackend.queries.DTO.ResponseDTO;
import com.pharmacybackend.queries.model.Queries;

public interface QueriesServices {
	
	public List<Queries> getQueries();
	public ResponseDTO addQueries(Queries query) throws Exception;
	public ResponseDTO updateQuery(Queries query,Integer queryId)throws Exception;

}
