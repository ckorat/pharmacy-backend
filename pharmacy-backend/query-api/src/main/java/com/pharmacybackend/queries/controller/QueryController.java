package com.pharmacybackend.queries.controller;

import javax.validation.Valid;

import com.pharmacybackend.queries.DTO.ResponseDTO;
import com.pharmacybackend.queries.model.Queries;
import com.pharmacybackend.queries.services.QueriesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;


@RestController
@RequestMapping (value ="/queries")
public class QueryController {
	@Autowired
	private QueriesServiceImpl queriesServiceImpl;
	@GetMapping("/")
	public ResponseEntity<ResponseDTO> getQueries(){
		return new ResponseEntity<>(new ResponseDTO("Fetched Successfully",queriesServiceImpl.getQueries()),HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<ResponseDTO>  saveQueries(@Valid @RequestBody Queries query)throws Exception {
		return new ResponseEntity<>(queriesServiceImpl.addQueries(query),HttpStatus.CREATED);
	}
	
	@PutMapping("/{queryId}")
	public ResponseEntity<ResponseDTO> updateQuery(@RequestBody(required = false) Queries query,@PathVariable("queryId") Integer queryId) throws Exception {
		return new ResponseEntity<>(queriesServiceImpl.updateQuery(query, queryId),HttpStatus.OK);
	}
}
