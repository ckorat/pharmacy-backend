package com.pharmacybackend.queries.DAO;

import java.util.List;

import com.pharmacybackend.queries.model.Queries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface QueriesDAO extends JpaRepository<Queries, Integer> {
	
	List<Queries> findByQueryStatus(@Param("queryStatus")Integer queryStatus);
	
	
}
