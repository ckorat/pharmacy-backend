package com.pharmacy.store.controller;


import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.exception.TechnicalException;
import com.pharmacy.store.dto.ResponseDTO;
import com.pharmacy.store.model.Medicine;
import com.pharmacy.store.model.Store;
import com.pharmacy.store.model.StorePassword;
import com.pharmacy.store.service.MedicineService;
import com.pharmacy.store.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

import java.util.Optional;

@RestController
@RequestMapping( "/api/v1/stores")
public class StoreController {
@Autowired
@Qualifier("storeService")
    StoreService storeService;
    @Autowired
    MedicineService medicineService;

    /*get mappings. if parameters like medicinename is given, this method will return all stores containing that medicine
      if pincode given it will list all stores within that pincode(Area), if none given it will return all stores.
    */
    @GetMapping("/")
    public ResponseEntity<ResponseDTO<List<Store>>> getAllStores(@RequestParam(required = false) Integer pincode, @RequestParam(required = false) String name) throws Exception{
        System.out.println(name +"controller");
        List<Store> storeList = storeService.getStore(name,pincode);
        return new ResponseEntity<>(new ResponseDTO<>(storeList),HttpStatus.OK);
    }

    //get store by storeid
    @GetMapping("/{storeid}")
    public ResponseEntity<ResponseDTO<Optional<Store>>> listStoreById(@PathVariable("storeid") int id) throws Exception
    {
        Optional<Store> storeList = storeService.getStoreById(id);
        return new ResponseEntity<>(new ResponseDTO<>(storeList),HttpStatus.OK);
    }

    //get medicines of particular store
    @GetMapping("/{storeid}/medicine")
    public ResponseEntity<ResponseDTO<List<Medicine>>> listMedicineByStoreId(@PathVariable("storeid") int id )throws Exception
    {
        List<Medicine> medicineList = medicineService.getMedicineByStoreId(id);
        return new ResponseEntity<>(new ResponseDTO<>(medicineList),HttpStatus.OK);
    }
    //get medicine by medicineid
    @GetMapping("/{storeid}/medicine/{medicineid}")
    public ResponseEntity<ResponseDTO<Optional<Medicine>>> listMedicineByMedicineId(@PathVariable("medicineid") int id )throws Exception
    {
        System.out.println(id);
        Optional<Medicine> medicineList = medicineService.getMedicineByMedicineId(id);
        return new ResponseEntity<>(new ResponseDTO<>(medicineList),HttpStatus.OK);
    }
    //Add store
    @PostMapping("/")
    public ResponseEntity<ResponseDTO<String>> addStore(@Valid @RequestBody Store store) throws MethodArgumentNotValidException, TechnicalException {
       storeService.addStore(store);
        return new ResponseEntity<>(new ResponseDTO<>("Store Saved Successfully"),HttpStatus.CREATED);
    }

    //Add medicine
    @PostMapping("/{storeid}/medicine")
    public ResponseEntity<ResponseDTO<String>> addMedicine( @PathVariable("storeid") int id,@Valid @RequestBody Medicine medicine) throws Exception
    {
        medicineService.addMedicine(medicine,id);
        return new ResponseEntity<>(new ResponseDTO<>("success","Medicine Saved Successfully"),HttpStatus.CREATED);
    }

    @PutMapping("/{storeid}")
    public ResponseEntity<ResponseDTO<String>> updateStore(@PathVariable("storeid") int id , @RequestBody Store store) throws Exception
    {
        if(!storeService.updateStore(store,id)){
            return new ResponseEntity<>(new ResponseDTO<>("Store Could not be Updated"),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ResponseDTO<>("Store Updated Successfully"),HttpStatus.OK);
    }
    //for store password change
    @PutMapping("/")
    public ResponseEntity<ResponseDTO> updatePassword(@RequestBody StorePassword data) throws ResourceNotFoundException {
        return new ResponseEntity<>(storeService.updateStorePassword(data),HttpStatus.OK);
    }

    @PutMapping("/{storeid}/medicine/{medicineid}")
    public ResponseEntity<ResponseDTO<String>> updateMedicineQuantity(@PathVariable("storeid") int id , @PathVariable("medicineid") int mid ,@RequestBody Medicine medicine)throws Exception
    {
        medicineService.updateMedicineQuantity(medicine,id,mid);
        return new ResponseEntity<>(new ResponseDTO<>("Stock Updated Successfully"),HttpStatus.OK);
    }

}
