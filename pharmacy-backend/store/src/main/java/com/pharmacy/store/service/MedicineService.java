package com.pharmacy.store.service;

import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.exception.TechnicalException;
import com.pharmacy.store.model.Medicine;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;
import java.util.Optional;


public interface MedicineService {

     Medicine addMedicine(Medicine medicine, int storeid) throws TechnicalException, MethodArgumentNotValidException, ResourceNotFoundException;
     List<Medicine> getMedicineByStoreId(int storeid) throws ResourceNotFoundException;
     Optional<Medicine> getMedicineByMedicineId(int medicineid) throws ResourceNotFoundException;
     Boolean updateMedicineQuantity(Medicine medicine,int storeid, int medicineid) throws Exception;


}
