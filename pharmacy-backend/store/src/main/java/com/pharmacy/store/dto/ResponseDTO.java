package com.pharmacy.store.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class ResponseDTO<T> {
    private String message;
    private T result;

    public ResponseDTO(String message) {
        this.message = message;
    }

    public ResponseDTO(T result) {
        this.result = result;
    }

    public ResponseDTO(String message, T result) {
        this.message = message;
        this.result = result;
    }
}
