package com.pharmacy.store.service;

import com.pharmacy.exception.TechnicalException;
import com.pharmacy.store.dto.ResponseDTO;
import com.pharmacy.store.exceptionHandler.ForbiddenAccessException;
import com.pharmacy.store.model.Store;
import com.pharmacy.store.model.StorePassword;
import com.pharmacy.store.repository.StoreRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import com.pharmacy.exception.PharmacyBaseException;
import com.pharmacy.exception.ResourceNotFoundException;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("storeService")
@AllArgsConstructor
public class StoreServiceImpl implements StoreService {
    @Autowired
    StoreRepository storeRepository;


    @Override
    public Store addStore(Store store) throws MethodArgumentNotValidException {
        return storeRepository.save(store);
    }

    public Optional<Store> getStoreById(int id)throws ResourceNotFoundException,ForbiddenAccessException {
        Optional<Store> store = storeRepository.findById(id);
        if (store.isPresent()) {
            Store storedetails = store.get();
            int storestatus = storedetails.getStatus();
            if (storestatus == 0) {
                throw new ForbiddenAccessException("Store with Id:" + id + " is not Activated");
            }
            return store;
        }
        throw new ResourceNotFoundException("No Store with this id exist");


    }

    public Boolean updateStore(Store store, int storeid) throws Exception {
        Optional<Store> storeforupdate = storeRepository.findById(storeid);
        if(storeforupdate.isPresent())
        {
            Store storedetails = storeforupdate.get();
            storedetails.setStoreName(store.getStoreName());
            storedetails.setStoreOwnerName(store.getStoreOwnerName());
            storedetails.setStoreAddress(store.getStoreAddress());
            storedetails.setStoreContactno(store.getStoreContactno());
            storedetails.setStoreEmailId(store.getStoreEmailId());
            storedetails.setStorePincode(store.getStorePincode());
            storedetails.setStatus(store.getStatus());
            storeRepository.save(storedetails);
            return true;
        }
        throw new ResourceNotFoundException("No Store with id: "+ storeid +" found");
    }



    @Override
    public List<Store> getStore(String medicinename, Integer pincode) throws Exception  {
        List<Store> storeList ;
        if(pincode != null)
        {
            storeList = storeRepository.getStoreByStorePincode(pincode);
            return storeList;
        }
        if(medicinename != null)
        {
            storeList = storeRepository.getStoreByMedicineName(medicinename);
            return storeList;
        }
        storeList = storeRepository.findAll();
        if(storeList.isEmpty())
        {
            throw new TechnicalException("Something went wrong");
        }
        return storeRepository.findAll();
    }

    public ResponseDTO updateStorePassword(StorePassword data) throws ResourceNotFoundException {
        return  storeRepository.findById(data.getStoreId()).map(store -> {

                    if (store.getStorePassword().equals(data.getOldPassword())) {
                        store.setStorePassword(data.getNewPassword());
                        storeRepository.save(store);
                        return new ResponseDTO("success", "Password updated");
                    }
                    return new ResponseDTO("error", "Old Password Is Wrong");
                }
        ).orElseThrow(()->new ResourceNotFoundException("Store not found"));

    }

}
