package com.pharmacy.store.repository;

import com.pharmacy.store.model.Medicine;
import com.pharmacy.store.model.Store;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MedicineRepository extends JpaRepository<Medicine,Integer> {

   List<Medicine> getMedicineByStoreId(int storeid);

    @Modifying
    @Transactional
    @Query(value = "update medicinemaster set medicinequantity = :qty where medicineid = :mid and storeid = :storeid",nativeQuery = true)
    int updateMedicineQty(int storeid , int mid , int qty);



}
