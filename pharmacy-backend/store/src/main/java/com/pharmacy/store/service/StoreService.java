package com.pharmacy.store.service;



import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.exception.TechnicalException;
import com.pharmacy.store.dto.ResponseDTO;
import com.pharmacy.store.model.Store;
import com.pharmacy.store.model.StorePassword;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;
import java.util.Optional;

public interface StoreService {
     Store addStore(Store store) throws MethodArgumentNotValidException, TechnicalException;
     Boolean updateStore(Store store , int storeid) throws Exception;
     ResponseDTO updateStorePassword(StorePassword storePassword) throws ResourceNotFoundException;
     Optional<Store> getStoreById(int storeid) throws Exception;
     List<Store> getStore(String medicinename, Integer pincode)throws Exception;

}
