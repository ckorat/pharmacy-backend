package com.pharmacy.store.repository;

import com.pharmacy.store.model.Medicine;
import com.pharmacy.store.model.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StoreRepository extends JpaRepository<Store,Integer> {
    List<Store> getStoreByStorePincode(int pincode);

    //find store by medicinename
    @Query(value = "SELECT s.*, m.* from Storedetails s, Medicinemaster m WHERE s.storeid=m.storeid and m.medicinename= :medicinename", nativeQuery = true)
    List<Store> getStoreByMedicineName(String medicinename);



}
