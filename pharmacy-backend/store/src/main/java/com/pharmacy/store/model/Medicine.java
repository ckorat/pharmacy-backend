package com.pharmacy.store.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DynamicUpdate
@Table(name="medicinemaster")
public class Medicine {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="medicineid")
    private int medicineId;

    @Column(name="storeid")
    private int storeId;

    @Column(name="medicinename")
    @JsonProperty("name")
    @NotEmpty(message = "Medicine Name should not be Empty")
    private String medicineName;

    @Column(name="medicinedescription")
    @JsonProperty("description")
    @NotEmpty(message = "Medicine Description should not be Empty")
    private String medicineDescription;

    @Column(name="companyname")
    @JsonProperty("company")
    @NotEmpty(message = "Company Name should not be Empty")
    private String companyName;

    @Column(name="medicinetype")
    @JsonProperty("type")
    @NotEmpty(message = "Medicine Type should not be Empty")
    private String medicineType;

    @Column(name="manufacturingdate" , columnDefinition="DATE")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @NotNull(message = "Medicine Manufacturing Date should not be Empty")
    @JsonProperty("manufacturingDate")
    private Date  manufacturingDate;

    @Column(name="expirydate", columnDefinition="DATE")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @NotNull(message = "Medicine Expiry Date should not be Empty")
    @JsonProperty("expiryDate")
    private Date expiryDate;

    @Column(name="medicinequantity")
    @JsonProperty("quantity")
    @NotNull(message = "Medicine Quantity should not be Empty")
    private int medicineQuantity;

    @Column(name="medicineprice")
    @JsonProperty("price")
    @NotNull(message = "Medicine Price should not be Empty")
    private double medicinePrice;

}
