package com.pharmacy.store.model;

import lombok.Data;

@Data
public class StorePassword {
    int storeId;
    String oldPassword;
    String newPassword;
}
