package com.pharmacy.store.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="storedetails")
public class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="storeid")
    @JsonProperty("storeId")

    private int storeId;

    @Column(name="storename")
    @JsonProperty("storeName")
    @NotEmpty(message = "Store Name must not be Empty")
    private String storeName;


    @Column(name="storeownername")
    @JsonProperty("storeOwnerName")
    @NotEmpty(message = "Store OwnerName must not be Empty")
    private String storeOwnerName;


    @Email(message = "email should be a valid email")
    @Column(name="storeemailid")
    @JsonProperty("storeEmailId")
    @NotEmpty(message = "Store Email must not be Empty")
    private String storeEmailId;


    @Column(name="storeaddress")
    @JsonProperty("storeAddress")
    @NotEmpty(message = "Store Address must not be Empty")
    private String storeAddress;

    @JsonProperty(value= "storePassword", access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="storepassword")
    @NotEmpty(message = "Password must not be Empty")
    private String storePassword;


    @Column(name="storepincode")
    @JsonProperty("storePincode")
    @NotNull(message = "Pincode must not be Empty")
    private int storePincode;



    @Column(name="storecontactno")
    @JsonProperty("storeContactno")
    @NotEmpty(message = "Store Contact Number must not be Empty")
    @Pattern(regexp="(^[0-9]{10})", message = "Store contact no should be numeric and should be of 10 digits length")
    private String storeContactno;

    @Column(name="status")
    @JsonProperty("status")
    private int status;


}
