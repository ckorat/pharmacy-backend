package com.pharmacy.store.service;



import com.pharmacy.exception.TechnicalException;
import com.pharmacy.store.model.Medicine;
import com.pharmacy.store.model.Store;
import com.pharmacy.store.repository.MedicineRepository;
import com.pharmacy.store.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pharmacy.exception.ResourceNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MedicineServiceImpl implements MedicineService {
    @Autowired
    MedicineRepository medicineRepository;

    @Autowired
    StoreRepository storeRepository;

    @Override
    public Medicine addMedicine(Medicine medicine, int storeid) throws MethodArgumentNotValidException, ResourceNotFoundException {
        Optional<Store> store = storeRepository.findById(storeid);
        if(!store.isPresent())
        {
            throw new ResourceNotFoundException("No Store Found");
        }
        medicine.setStoreId(storeid);
        return medicineRepository.save(medicine);
    }

    @Override
    public List<Medicine> getMedicineByStoreId(int storeid) throws ResourceNotFoundException {
        List<Medicine> medicine = medicineRepository.getMedicineByStoreId(storeid);
        if(medicine.isEmpty())
        {
            throw new ResourceNotFoundException("Wrong Store Id or this Store Id does not contains Medicines");
        }
        return medicine;
    }

    @Override
    public Optional<Medicine> getMedicineByMedicineId(int medicineid) throws ResourceNotFoundException {
        System.out.println(medicineid);
        Optional<Medicine> medicine = medicineRepository.findById(medicineid);
        if(!medicine.isPresent())
        {
            throw new ResourceNotFoundException("Wrong Store Id or this Store Id does not contains Medicines");
        }
        System.out.println(medicine);
        return medicine;
    }

    @Override
   public Boolean updateMedicineQuantity(Medicine medicine,int storeid,int medicineid) throws ResourceNotFoundException {
        int affectedRows = medicineRepository.updateMedicineQty(storeid,medicineid,medicine.getMedicineQuantity());
        if(affectedRows == 0)
        {
            throw new ResourceNotFoundException("Something went wrong");
        }
        return true;
    }
}
