package com.pharmacy.store.Service;

import com.pharmacy.exception.PharmacyBaseException;
import com.pharmacy.exception.ResourceNotFoundException;
import com.pharmacy.store.exceptionHandler.ForbiddenAccessException;
import com.pharmacy.store.model.Store;
import com.pharmacy.store.repository.StoreRepository;
import com.pharmacy.store.service.StoreService;
import com.pharmacy.store.service.StoreServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class StoreServiceTest {

    StoreServiceImpl storeService;
    StoreRepository storeRepository = mock(StoreRepository.class);

    @Before
    public void setup()
    {
        storeService = new StoreServiceImpl(storeRepository);
    }

    @Test
    public void getAllStoreSuccess() throws Exception {
        Store store = new Store(38,"Store1","Mr.Verma","store1@gmail.com","pune","123456789",395009,"9874563214",1);
        List<Store> storeList = new ArrayList<>();
        storeList.add(store);
        when(storeRepository.findAll()).thenReturn(storeList);
        when(storeRepository.getStoreByMedicineName(anyString())).thenReturn(storeList);
        when(storeRepository.getStoreByStorePincode(anyInt())).thenReturn(storeList);
        List<Store> storeList1 = storeService.getStore("mofilet",395009);
        assertEquals(storeList,storeList1);
    }

    @Test
    public void getStoreByIdSuccess() throws ResourceNotFoundException, ForbiddenAccessException {
        Store store = new Store(38,"Store1","Mr.Verma","store1@gmail.com","pune","123456789",395009,"9874563214",1);
        Optional<Store> storeListExpected = Optional.of(store);
        when(storeRepository.findById(anyInt())).thenReturn(storeListExpected);
        Optional<Store> storeListActual = storeService.getStoreById(39);
        assertEquals(storeListExpected,storeListActual);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void getStoreByIdFailure() throws ResourceNotFoundException, ForbiddenAccessException {
        Mockito.when(storeService.getStoreById(39)).thenThrow(new ResourceNotFoundException("No Resource found"));;
    }
}
