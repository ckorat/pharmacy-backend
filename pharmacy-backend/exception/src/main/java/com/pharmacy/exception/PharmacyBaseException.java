package com.pharmacy.exception;

import org.springframework.http.HttpStatus;

import java.util.List;

public class PharmacyBaseException extends Exception {
    private HttpStatus httpStatusCode;
    private String message;
    private List<String> details;

    public PharmacyBaseException(HttpStatus httpStatusCode, String message, List<String> details) {
        this.httpStatusCode = httpStatusCode;
        this.message = message;
        this.details = details;
    }
    public PharmacyBaseException(HttpStatus httpStatusCode, String message)
    {
        this.httpStatusCode = httpStatusCode;
        this.message = message;
    }
    public PharmacyBaseException(HttpStatus httpStatusCode,List<String> details)
    {
        this.httpStatusCode=httpStatusCode;
        this.details = details;
    }

    public HttpStatus getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(HttpStatus httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "PharmacyBaseException{" +
                "httpStatusCode=" + httpStatusCode +
                ", message='" + message + '\'' +
                ", details=" + details +
                '}';
    }
}
