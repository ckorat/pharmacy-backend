package com.pharmacy.exception;

import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.function.Supplier;

public class ResourceNotFoundException extends PharmacyBaseException  {
    public ResourceNotFoundException(String message, List<String> details) {
        super(HttpStatus.NOT_FOUND, message, details);
    }

    public ResourceNotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }

    public ResourceNotFoundException(List<String> details) {
        super(HttpStatus.NOT_FOUND, details);
    }


}
