package com.pharmacy.exception;

import java.util.List;
import org.springframework.http.HttpStatus;
public class TechnicalException extends PharmacyBaseException {
    public TechnicalException(String message, List<String> details) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, message, details);
    }

    public TechnicalException(String message) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, message);
    }

    public TechnicalException( List<String> details) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, details);
    }
}
